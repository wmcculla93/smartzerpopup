# Smartzer Pop Up - Code Challenge

## Features

The app is as asked for: Click a button and the pop up will appear with the layout specified. I have made one
change to the initial design: In portrait orientation, the images appear rather than the radio buttons. The
rest is as per the design (and fully responsive).

The pop up is scalable and dynamic. In the absense of an API or DB to call against, I built a JSON file to act in their stead.
This pulls the data from the JSON file and fills in the data on the popup. As well, the app meets a11y standards. The text is 
fully scalable and the entire thing is keyboard navigable.

## Technologies Used

Languages used: React.js, JS, CSS, HTML

IDE: Notepad++
