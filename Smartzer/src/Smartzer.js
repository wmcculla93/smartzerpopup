import React from 'react';
import './Smartzer.css';
import productInfo from './productInfo.json';

class Smartzer extends React.Component {
  constructor() {
    super();
	
	this.state = {
      buttonClickable: 0,
	  showPopup: false
	}
  }
  
  
  //Toggle popup showing or not
  showHidePopup = event => {
	  this.setState({
		  buttonClickable: -1,
		  showPopup: !this.state.showPopup
	  })
  }
  
  render() {
	return (
	  <div>
	  <div className="Product-button-cont">
	    <h1>
	      Smartzer Pop Up Code Challenge
	    </h1>
		<h3>
		  Thanks in advance for your consideration!
		  <br />
		  William
		</h3>
		<button className="Product-clickme" onClick={this.showHidePopup} tabIndex={this.state.buttonClickable}>
	      Click Me!
	    </button>
	  </div>
	  {this.state.showPopup ?
        <Popup exitHandle={this.showHidePopup} /> :
        null
      }
	  </div>
    )
  }
}

class Popup extends React.Component {

  constructor() {
    super();	
	
	this.state = {
	  currentImage: 0
	}
  }
  
  // Pull data from json file, insert images dependent on how many img links there are
  renderImages() {
	let galleryArray = []
	for (let i = 0; i < productInfo.imgs.length; i++) {		
	  galleryArray.push(<Thumbnail galleryID={i} key={i} clickHandle={this.changeImage} classUpdate={this.isActive} />)
	}
	return galleryArray
  }
  
  
  // Check if image is currently selected or not
  isActive = (imgSelected) => {	    
	if (this.state.currentImage == imgSelected) {
      return 'Product-image-selected'
	}
	else {
      return ""
	}
  }
  
  // When user clicks on thumbnail 
  changeImage = event => {	  	  
    this.setState({
	  currentImage: event.target.getAttribute('gallery-id')
    })
  }
	
  render() {
	return (
	<div className="Product-body-wrapper">
	  <div className='Product-body-container'>
		<a href="#" onClick={this.props.exitHandle}>
	      <div className="Product-exit" >
		  </div>
		</a>
	    <div className='Product-container'>
          <div className='Product-image-container'>
	        <img src={productInfo.imgs[this.state.currentImage].src} alt={productInfo.name} className="Product-image-display" />
		  </div>
		  <div className='Product-gallery-container'>
	        {this.renderImages()}
		  </div>
        </div>
		<div className='Product-container Product-container-text'>
		  <h1 className="Product-name">{productInfo.name}</h1>
		  <div className="Product-discovery-container">
		    <a href="#" className='Product-discovery'>
		      Discover
		    </a>
		  </div>
		  <div className="Product-description">
		    <h3>
		      Description
		    </h3>
		    <p>
		      {productInfo.disc}
	        </p>
	      </div>
	    </div>
	  </div>
	</div>
    );
  }
}

// Generate Thumnail Image
class Thumbnail extends React.Component {
  
  render() {
	return (
	  <div className="Product-thumbnail-container" key={this.props.galleryID}>
	    <div className={[this.props.classUpdate(this.props.galleryID), "Product-image-selector"].join(' ')}></div>
	    <a href="#" gallery-id={this.props.galleryID} onClick={this.props.clickHandle}>
	      <img src={productInfo.imgs[this.props.galleryID].src} alt={productInfo.name} gallery-id={this.props.galleryID} className={[this.props.classUpdate(this.props.galleryID), "Product-image-thumbnail"].join(' ')} />
	    </a>
	  </div>
    )
  }
}

export default Smartzer;
